package imt3673.mobdev_lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class A3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);
        findViewById(R.id.B3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendT4Back();
            }
        });
    }

    @Override
    public void onBackPressed() {
        sendT4Back();
    }

    public void sendT4Back() {
        EditText T4 = findViewById(R.id.T4);
        String T4Value = T4.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("T4Value", T4Value);
        setResult(RESULT_OK, intent);
        finish();
    }
}
