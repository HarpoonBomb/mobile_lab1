package imt3673.mobdev_lab1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class A1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        //Drop down list
        Spinner L1 = findViewById(R.id.L1);
        String[] list = new String[]{"Item 1", "Item 2", "Item 3"};
        ArrayAdapter<String> listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, list);
        L1.setAdapter(listAdapter);
        //Restore L1 state
        SharedPreferences L1Preferences = this.getSharedPreferences("L1Preferences", MODE_PRIVATE);
        String L1Value = L1Preferences.getString("L1_selected", "");
        L1.setSelection(listAdapter.getPosition(L1Value));

        //Button
        findViewById(R.id.B1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText T1 =  findViewById(R.id.T1);
                String T1Value = T1.getText().toString();
                startActivity(new Intent(A1.this, A2.class).putExtra("T1Value", T1Value));
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Saving L1 state
        Spinner L1 = findViewById(R.id.L1);
        SharedPreferences L1Preferences = this.getSharedPreferences("L1Preferences", MODE_PRIVATE);
        SharedPreferences.Editor PreferencesEditor = L1Preferences.edit();
        PreferencesEditor.putString("L1_selected", L1.getSelectedItem().toString());
        PreferencesEditor.apply();
    }
}
