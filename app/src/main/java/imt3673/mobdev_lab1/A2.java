package imt3673.mobdev_lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class A2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);

        //T2
        TextView T2 = findViewById(R.id.T2);
        String T1Value = getIntent().getStringExtra("T1Value");
        T2.setText("Hello " + T1Value);

        //Button
        findViewById(R.id.B2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(A2.this, A3.class), 1);
                //startActivity(new Intent(A2.this, A3.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        TextView T3 = findViewById(R.id.T3);
        String T4Value = data.getStringExtra("T4Value");
        T3.setText("From A3: " + T4Value);
    }
}
